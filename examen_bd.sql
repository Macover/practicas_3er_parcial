-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-04-2021 a las 03:42:12
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `examen_bd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas_usuarios`
--

CREATE TABLE `respuestas_usuarios` (
  `id_users_answers` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `genero` varchar(25) NOT NULL,
  `edad` int(10) NOT NULL,
  `puntaje` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `respuestas_usuarios`
--

INSERT INTO `respuestas_usuarios` (`id_users_answers`, `id_usuario`, `genero`, `edad`, `puntaje`, `created_at`, `updated_at`) VALUES
(58, 13, 'masculino', 20, 1, '2021-03-31 00:00:01', '2021-03-31 00:00:01'),
(59, 14, 'femenino', 21, 2, '2021-03-31 00:01:18', '2021-03-31 00:01:18'),
(60, 15, 'masculino', 34, 2, '2021-03-31 00:40:23', '2021-03-31 00:40:23'),
(61, 16, 'masculino', 32, 2, '2021-03-31 00:42:23', '2021-03-31 00:42:23'),
(62, 17, 'femenino', 45, 3, '2021-03-31 00:44:02', '2021-03-31 00:44:02'),
(63, 18, 'masculino', 45, 1, '2021-03-31 00:44:46', '2021-03-31 00:44:46'),
(64, 19, 'femenino', 54, 4, '2021-03-31 00:45:46', '2021-03-31 00:45:46'),
(65, 20, 'femenino', 32, 3, '2021-03-31 00:46:46', '2021-03-31 00:46:46'),
(66, 21, 'masculino', 64, 4, '2021-03-31 00:48:29', '2021-03-31 00:48:29'),
(67, 22, 'femenino', 26, 5, '2021-03-31 00:49:21', '2021-03-31 00:49:21'),
(68, 23, 'masculino', 12, 0, '2021-03-31 05:09:42', '2021-03-31 05:09:42'),
(69, 24, 'masculino', 54, 1, '2021-04-02 07:40:00', '2021-04-02 07:40:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `nombre_usuario` varchar(50) NOT NULL,
  `edad` int(10) NOT NULL,
  `genero` varchar(25) NOT NULL,
  `correo` varchar(70) NOT NULL,
  `contrasenia` varchar(80) NOT NULL,
  `tipo_usuario` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre_usuario`, `edad`, `genero`, `correo`, `contrasenia`, `tipo_usuario`, `created_at`, `updated_at`) VALUES
(5, 'root', 0, '', 'admin@root', '$2y$10$vk9QQ.c0CaJZpEuwPLx0v.IsNCWz/msXcZCxyAInKLv2yERkIqZ0G', 'admin', '2021-03-27 23:34:13', '2021-03-27 23:34:13'),
(13, 'Isaí', 20, 'masculino', 'isai@correo.com', '$2y$10$vLuPMvpfRlsOi7a29gnNOOObkfiwaFYbtU7.jasLNXM/JxCrSobn6', 'user', '2021-03-30 23:45:37', '2021-03-30 23:45:37'),
(14, 'Fernanda', 21, 'femenino', 'fernanda@correo.com', '$2y$10$hfJDJaXarTYNWrh1z7EClOgpPo71jqLA0tMqSvo67o13S6FFsNoAC', 'user', '2021-03-30 23:46:05', '2021-03-30 23:46:05'),
(15, 'Oscar', 34, 'masculino', 'oscar@correo.com', '$2y$10$1UrNkYOyHAmo3GF2WM5XteuAMZlUvJCXN6gG7zll5eW5SRMS7gEk2', 'user', '2021-03-31 00:40:10', '2021-03-31 00:40:10'),
(16, 'Martin', 32, 'masculino', 'martin@correo.com', '$2y$10$2E.cin0rrH2K3ZktEoWhF.LJkUW/Be2gFP47bD3YYnNbKL3yECHt6', 'user', '2021-03-31 00:42:08', '2021-03-31 00:42:08'),
(17, 'Maria', 45, 'femenino', 'maria@correo.com', '$2y$10$rK659J50JyHFQOAc0IHqU.QWLBqj0LstLwErgxxQXeCIFiazMU7UC', 'user', '2021-03-31 00:43:34', '2021-03-31 00:43:34'),
(18, 'Jose', 45, 'masculino', 'jose@correo.com', '$2y$10$vthA1AvK/4EYGyNNV1clrO.IHyLK9hzZNi/z6Nequ6kWcNbN8O41a', 'user', '2021-03-31 00:44:35', '2021-03-31 00:44:35'),
(19, 'Joana', 54, 'femenino', 'joana@correo.com', '$2y$10$45nY69ZEyKJDjpPZKm9ATuICQ.lZVfo2arCTClqWmV0FSeGtTg4qS', 'user', '2021-03-31 00:45:16', '2021-03-31 00:45:16'),
(20, 'Karla', 32, 'femenino', 'karla@correo.com', '$2y$10$QUMe7WYUF.SZi7xZoAyPe.o76PtzM5hgoWeI4qxvyykIe54uRMZSG', 'user', '2021-03-31 00:46:35', '2021-03-31 00:46:35'),
(21, 'Carlos', 64, 'masculino', 'carlos@correo.com', '$2y$10$mwmgjOEYz.fBcAbYYkdRIOj7E2Ji/KUgl/yKb6CM8ant8ZVtM2X9a', 'user', '2021-03-31 00:48:09', '2021-03-31 00:48:09'),
(22, 'Julia', 26, 'femenino', 'julia@correo.com', '$2y$10$0uBjJZI.sDoSDmpsaErnaeIxPdamMRRbZ/NKaZ2z1QAomHKrfNNKS', 'user', '2021-03-31 00:48:57', '2021-03-31 00:48:57'),
(23, 'Fernando', 12, 'masculino', 'fernando@correo.com', '$2y$10$fHxJMfb4hTP7YNTReTH3XeNrNMraPiP6fVODhhu0Bvz3y74G67fg2', 'user', '2021-03-31 05:09:10', '2021-03-31 05:09:10'),
(24, 'Justin', 54, 'masculino', 'Justin@correo.com', '$2y$10$RFEpLs3sgkBK.jlW.TK8YumNEPB2/r/z1zzkoNZf857r4fR7Ikg5K', 'user', '2021-04-02 07:39:41', '2021-04-02 07:39:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_respuesta`
--

CREATE TABLE `usuario_respuesta` (
  `id_usuario_respuesta` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `numero_pregunta` varchar(25) NOT NULL,
  `usuario_respuesta` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario_respuesta`
--

INSERT INTO `usuario_respuesta` (`id_usuario_respuesta`, `id_usuario`, `numero_pregunta`, `usuario_respuesta`, `created_at`, `updated_at`) VALUES
(194, 13, 'pregunta1', 'b', '2021-03-31 00:00:01', '2021-03-31 00:00:01'),
(195, 13, 'pregunta2', 'c', '2021-03-31 00:00:01', '2021-03-31 00:00:01'),
(196, 13, 'pregunta3', 'c', '2021-03-31 00:00:01', '2021-03-31 00:00:01'),
(197, 13, 'pregunta4', 'b', '2021-03-31 00:00:01', '2021-03-31 00:00:01'),
(198, 13, 'pregunta5', 'b', '2021-03-31 00:00:01', '2021-03-31 00:00:01'),
(199, 14, 'pregunta1', 'b', '2021-03-31 00:01:18', '2021-03-31 00:01:18'),
(200, 14, 'pregunta2', 'c', '2021-03-31 00:01:18', '2021-03-31 00:01:18'),
(201, 14, 'pregunta3', 'b', '2021-03-31 00:01:18', '2021-03-31 00:01:18'),
(202, 14, 'pregunta4', 'b', '2021-03-31 00:01:18', '2021-03-31 00:01:18'),
(203, 14, 'pregunta5', 'c', '2021-03-31 00:01:18', '2021-03-31 00:01:18'),
(204, 15, 'pregunta1', 'b', '2021-03-31 00:40:23', '2021-03-31 00:40:23'),
(205, 15, 'pregunta2', 'c', '2021-03-31 00:40:23', '2021-03-31 00:40:23'),
(206, 15, 'pregunta3', 'c', '2021-03-31 00:40:23', '2021-03-31 00:40:23'),
(207, 15, 'pregunta4', 'b', '2021-03-31 00:40:23', '2021-03-31 00:40:23'),
(208, 15, 'pregunta5', 'c', '2021-03-31 00:40:23', '2021-03-31 00:40:23'),
(209, 16, 'pregunta1', 'c', '2021-03-31 00:42:23', '2021-03-31 00:42:23'),
(210, 16, 'pregunta2', 'c', '2021-03-31 00:42:23', '2021-03-31 00:42:23'),
(211, 16, 'pregunta3', 'c', '2021-03-31 00:42:23', '2021-03-31 00:42:23'),
(212, 16, 'pregunta4', 'b', '2021-03-31 00:42:23', '2021-03-31 00:42:23'),
(213, 16, 'pregunta5', 'a', '2021-03-31 00:42:23', '2021-03-31 00:42:23'),
(214, 17, 'pregunta1', 'c', '2021-03-31 00:44:02', '2021-03-31 00:44:02'),
(215, 17, 'pregunta2', 'a', '2021-03-31 00:44:02', '2021-03-31 00:44:02'),
(216, 17, 'pregunta3', 'a', '2021-03-31 00:44:02', '2021-03-31 00:44:02'),
(217, 17, 'pregunta4', 'a', '2021-03-31 00:44:02', '2021-03-31 00:44:02'),
(218, 17, 'pregunta5', 'a', '2021-03-31 00:44:02', '2021-03-31 00:44:02'),
(219, 18, 'pregunta1', 'b', '2021-03-31 00:44:46', '2021-03-31 00:44:46'),
(220, 18, 'pregunta2', 'b', '2021-03-31 00:44:46', '2021-03-31 00:44:46'),
(221, 18, 'pregunta3', 'a', '2021-03-31 00:44:46', '2021-03-31 00:44:46'),
(222, 18, 'pregunta4', 'a', '2021-03-31 00:44:46', '2021-03-31 00:44:46'),
(223, 18, 'pregunta5', 'a', '2021-03-31 00:44:46', '2021-03-31 00:44:46'),
(224, 19, 'pregunta1', 'c', '2021-03-31 00:45:46', '2021-03-31 00:45:46'),
(225, 19, 'pregunta2', 'a', '2021-03-31 00:45:46', '2021-03-31 00:45:46'),
(226, 19, 'pregunta3', 'a', '2021-03-31 00:45:46', '2021-03-31 00:45:46'),
(227, 19, 'pregunta4', 'b', '2021-03-31 00:45:46', '2021-03-31 00:45:46'),
(228, 19, 'pregunta5', 'a', '2021-03-31 00:45:46', '2021-03-31 00:45:46'),
(229, 20, 'pregunta1', 'b', '2021-03-31 00:46:46', '2021-03-31 00:46:46'),
(230, 20, 'pregunta2', 'a', '2021-03-31 00:46:46', '2021-03-31 00:46:46'),
(231, 20, 'pregunta3', 'a', '2021-03-31 00:46:46', '2021-03-31 00:46:46'),
(232, 20, 'pregunta4', 'a', '2021-03-31 00:46:46', '2021-03-31 00:46:46'),
(233, 20, 'pregunta5', 'c', '2021-03-31 00:46:46', '2021-03-31 00:46:46'),
(234, 21, 'pregunta1', 'c', '2021-03-31 00:48:29', '2021-03-31 00:48:29'),
(235, 21, 'pregunta2', 'a', '2021-03-31 00:48:29', '2021-03-31 00:48:29'),
(236, 21, 'pregunta3', 'a', '2021-03-31 00:48:29', '2021-03-31 00:48:29'),
(237, 21, 'pregunta4', 'b', '2021-03-31 00:48:29', '2021-03-31 00:48:29'),
(238, 21, 'pregunta5', 'b', '2021-03-31 00:48:29', '2021-03-31 00:48:29'),
(239, 22, 'pregunta1', 'c', '2021-03-31 00:49:21', '2021-03-31 00:49:21'),
(240, 22, 'pregunta2', 'a', '2021-03-31 00:49:21', '2021-03-31 00:49:21'),
(241, 22, 'pregunta3', 'a', '2021-03-31 00:49:21', '2021-03-31 00:49:21'),
(242, 22, 'pregunta4', 'b', '2021-03-31 00:49:21', '2021-03-31 00:49:21'),
(243, 22, 'pregunta5', 'c', '2021-03-31 00:49:21', '2021-03-31 00:49:21'),
(244, 23, 'pregunta1', 'b', '2021-03-31 05:09:42', '2021-03-31 05:09:42'),
(245, 23, 'pregunta2', 'b', '2021-03-31 05:09:42', '2021-03-31 05:09:42'),
(246, 23, 'pregunta3', 'b', '2021-03-31 05:09:42', '2021-03-31 05:09:42'),
(247, 23, 'pregunta4', 'a', '2021-03-31 05:09:42', '2021-03-31 05:09:42'),
(248, 23, 'pregunta5', 'a', '2021-03-31 05:09:42', '2021-03-31 05:09:42'),
(249, 24, 'pregunta1', 'b', '2021-04-02 07:40:00', '2021-04-02 07:40:00'),
(250, 24, 'pregunta2', 'c', '2021-04-02 07:40:00', '2021-04-02 07:40:00'),
(251, 24, 'pregunta3', 'b', '2021-04-02 07:40:00', '2021-04-02 07:40:00'),
(252, 24, 'pregunta4', 'b', '2021-04-02 07:40:00', '2021-04-02 07:40:00'),
(253, 24, 'pregunta5', 'b', '2021-04-02 07:40:00', '2021-04-02 07:40:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `respuestas_usuarios`
--
ALTER TABLE `respuestas_usuarios`
  ADD PRIMARY KEY (`id_users_answers`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `usuario_respuesta`
--
ALTER TABLE `usuario_respuesta`
  ADD PRIMARY KEY (`id_usuario_respuesta`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `respuestas_usuarios`
--
ALTER TABLE `respuestas_usuarios`
  MODIFY `id_users_answers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `usuario_respuesta`
--
ALTER TABLE `usuario_respuesta`
  MODIFY `id_usuario_respuesta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=254;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `respuestas_usuarios`
--
ALTER TABLE `respuestas_usuarios`
  ADD CONSTRAINT `respuestas_usuarios_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario_respuesta`
--
ALTER TABLE `usuario_respuesta`
  ADD CONSTRAINT `usuario_respuesta_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
