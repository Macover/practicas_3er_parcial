-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-04-2021 a las 03:31:40
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda_online`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito_usuario`
--

CREATE TABLE `carrito_usuario` (
  `id_carrito_usuario` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `nombre_producto` varchar(50) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `carrito_usuario`
--

INSERT INTO `carrito_usuario` (`id_carrito_usuario`, `id_usuario`, `nombre_producto`, `id_producto`, `cantidad`, `total`, `created_at`, `updated_at`) VALUES
(140, 4, 'Playera Nike Sportswear', 20, 3, 1050, '2021-04-02 07:24:38', '2021-04-02 07:24:38'),
(141, 6, 'Gorra Nike Dri-FIT', 25, 1, 449, '2021-04-02 07:26:49', '2021-04-02 07:26:49'),
(142, 6, 'Tenis Under Armour Breathe Sola', 29, 1, 1619, '2021-04-02 07:26:49', '2021-04-02 07:26:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metodo_pago`
--

CREATE TABLE `metodo_pago` (
  `id_metodo_pago` int(11) NOT NULL,
  `titular` varchar(70) NOT NULL,
  `no_tarjeta` varchar(50) NOT NULL,
  `expira` varchar(50) NOT NULL,
  `cvv` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `metodo_pago`
--

INSERT INTO `metodo_pago` (`id_metodo_pago`, `titular`, `no_tarjeta`, `expira`, `cvv`, `created_at`, `updated_at`) VALUES
(31, 'Titular', '1234234556123456', '2021-04-14', 123, '2021-04-02 07:25:38', '2021-04-02 07:25:38'),
(32, 'Titularr', '3465234556123456', '2021-04-30', 547, '2021-04-02 07:28:45', '2021-04-02 07:28:45'),
(33, 'Titularr', '3465234556123456', '2021-04-30', 547, '2021-04-02 07:30:20', '2021-04-02 07:30:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos_usuario`
--

CREATE TABLE `pedidos_usuario` (
  `id_pedido_usuario` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `nombre_producto` varchar(70) NOT NULL,
  `precio` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `id_metodo_pago` int(11) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedidos_usuario`
--

INSERT INTO `pedidos_usuario` (`id_pedido_usuario`, `id_usuario`, `id_producto`, `nombre_producto`, `precio`, `cantidad`, `id_metodo_pago`, `direccion`, `created_at`, `updated_at`) VALUES
(26, 4, 20, 'Playera Nike Sportswear', 1050, 3, 31, 'DireccionPrueba', '2021-04-02 07:25:38', '2021-04-02 07:25:38'),
(27, 6, 25, 'Gorra Nike Dri-FIT', 449, 1, 32, 'Direccion2', '2021-04-02 07:28:45', '2021-04-02 07:28:45'),
(28, 6, 25, 'Gorra Nike Dri-FIT', 449, 1, 32, 'Direccion2', '2021-04-02 07:30:20', '2021-04-02 07:30:20'),
(29, 6, 29, 'Tenis Under Armour Breathe Sola', 1619, 1, 32, 'Direccion2', '2021-04-02 07:30:20', '2021-04-02 07:30:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_producto` int(11) NOT NULL,
  `nombre_producto` varchar(50) NOT NULL,
  `precio` int(12) NOT NULL,
  `descripcion` text NOT NULL,
  `ruta_imagen1` varchar(100) NOT NULL,
  `ruta_imagen2` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `nombre_producto`, `precio`, `descripcion`, `ruta_imagen1`, `ruta_imagen2`, `created_at`, `updated_at`) VALUES
(19, 'Short Under Armour Raid', 359, 'El SHORT RAID de UNDER ARMOUR es el short ideal para que puedas llegar al limite', '/storage/productos/iILlf10svv4Ogb1xTJDfltsayBpQCLRb6fCTmFF2.jpg', '/storage/productos/9csFRdcpmzhTSWEHx9xwAfHZKgZX2JFtZiI3gV55.jpg', '2021-04-02 07:00:48', '2021-04-02 07:00:48'),
(20, 'Playera Nike Sportswear', 350, 'Un estilo innovador para tu hija.', '/storage/productos/lJhbSoWfMhD3HZVnza2zYEWYswX8coBNV8ed9N3S.jpg', '/storage/productos/biA9UsLxeq0QVfVGQpk4PlviKQiAGlk90xpmeUf7.jpg', '2021-04-02 07:04:26', '2021-04-02 07:04:26'),
(21, 'Jersey Charly Veracruz', 505, 'Tu hijo puede formar parte de la afición de los Tiburones Rojos', '/storage/productos/5P9T8jprvEhjRMl031okBWTRo7vUE0UDId4WGSUr.jpg', '/storage/productos/7kMvbAq4HoGrKdfBifnRs8OIaD71UPPJqkurFCC4.jpg', '2021-04-02 07:06:25', '2021-04-02 07:06:25'),
(22, 'Tenis Nike SpeedRep', 1899, 'Un equipamiento que soporte todos tus movimientos', '/storage/productos/rDEDqHyGmZx6DwaM2Ix0MVFuxwXH5Kt0lnd1JlgO.jpg', '/storage/productos/bmA4KcvKzIrziowEgr0uKFUNnAsvvg7B8QIzfMVQ.jpg', '2021-04-02 07:09:48', '2021-04-02 07:09:48'),
(23, 'Playera adidas Training Heat.RDY', 749, 'Elige la Playera Adidas Training Heat.RDY para tu rutina', '/storage/productos/h8aue1guabvDRwEXxIvt50qkJ2GGTqJVvgWzClXH.jpg', '/storage/productos/zpixAY7A8cgBkOm70QJcRCL7gfG7g7XepemGcENQ.jpg', '2021-04-02 07:11:35', '2021-04-02 07:11:35'),
(24, 'Short Nike Dri-FIT Attack', 549, 'El Short Nike Dri-FIT Attack de mujer es tan liviano', '/storage/productos/N5RA4sUXCPdngoh9mcaIEt2gPLK4SgJRe1FA9nM9.jpg', '/storage/productos/YG92sTCbt84EezoIzw2TXU6ozEJePqlargZC21sD.jpg', '2021-04-02 07:12:53', '2021-04-02 07:12:53'),
(25, 'Gorra Nike Dri-FIT', 449, 'La Gorra Nike Dri-FIT de niños tiene la comodidad para practicar cualquier deporte', '/storage/productos/2WlUeyaF8jpB05QXNtm9v2tUPK0mgaeLshCaXIQ9.jpg', '/storage/productos/gshBnXozOeuN1XAgrQB40txhlm1s7R93b73SS2Et.jpg', '2021-04-02 07:14:12', '2021-04-02 07:14:12'),
(26, 'Jersey Puma Liga Hooped', 239, 'Un diseño moderno y cómodo para destacar en cada partido', '/storage/productos/RK3Ut1ltMcc2o66mFQJG1PZ8chreJZUM5xCebJwz.jpg', '/storage/productos/LMOj2v06JV2hdKUwkNluY5xoO75ZBUGH7M8qskuJ.jpg', '2021-04-02 07:15:53', '2021-04-02 07:15:53'),
(27, 'Jersey Puma Chivas Femenil Visita 20/21', 979, 'Luce increíble portando con orgullo este Jersey Puma Chivas Femenil Visita 20/21', '/storage/productos/Fp7rp1EiQpilcMmiVUpAfhG3lrXFg5B9UDjsKkh4.jpg', '/storage/productos/SXdJ2RGNBVR721nf8DDcGZWX0OWeqw9ogOEKjBmH.jpg', '2021-04-02 07:18:02', '2021-04-02 07:18:02'),
(28, 'Playera Tank adidas Badge of Sport', 499, 'Aumenta el nivel de intensidad de tus entrenamientos usando la Playera Badge', '/storage/productos/hpIGuQwXawGS27MmjPTz6rciju4cXJZCnaWEHj3W.jpg', '/storage/productos/7dFduR2SYTCeFGKiR4pwVHuz7zH41RYVsceesO3a.jpg', '2021-04-02 07:19:49', '2021-04-02 07:19:49'),
(29, 'Tenis Under Armour Breathe Sola', 1619, 'Experimenta un recorrido suave con los tenis', '/storage/productos/0G1qYLu4Z3MwJ0W0jIAbFAYpuLu3kqSnEOqJ9aqK.jpg', '/storage/productos/LsyGF1jiX6SklKIcC1lUMeCnm1lBGwfdmrZGBpUL.jpg', '2021-04-02 07:21:17', '2021-04-02 07:21:17'),
(30, 'Tenis Under Armour Essential', 1099, 'Los Tenis Under Armour Essential  son perfectos para niños', '/storage/productos/mPDajcu6V4vcoYKWpx6FTLcPn6II5fhmCsuJlqLK.jpg', '/storage/productos/fbDSFrK7guc4BRc3dDQWhvxg9NW5gBUybbi9tlET.jpg', '2021-04-02 07:22:27', '2021-04-02 07:22:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `nombre_usuario` varchar(50) NOT NULL,
  `correo` varchar(70) NOT NULL,
  `contrasenia` varchar(80) NOT NULL,
  `tipo_usuario` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre_usuario`, `correo`, `contrasenia`, `tipo_usuario`, `created_at`, `updated_at`) VALUES
(4, '1234', '1234@1234', '$2y$10$OAHVQZNhsBARuy0fobrsrO20TmTwH7xrK6Qu/kWXPkMHSRi9RGTLq', 'user', '2021-03-27 21:16:44', '2021-03-27 21:16:44'),
(5, 'root', 'admin@root', '$2y$10$vk9QQ.c0CaJZpEuwPLx0v.IsNCWz/msXcZCxyAInKLv2yERkIqZ0G', 'admin', '2021-03-27 23:34:13', '2021-03-27 23:34:13'),
(6, '12345', '12345@12345', '$2y$10$COnRFyoAlVUSVo93Fma/3eJLz8bde3S3lLoED9rs9ObpdC5qT09Ai', 'user', '2021-03-28 00:19:00', '2021-03-28 00:19:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `carrito_usuario`
--
ALTER TABLE `carrito_usuario`
  ADD PRIMARY KEY (`id_carrito_usuario`);

--
-- Indices de la tabla `metodo_pago`
--
ALTER TABLE `metodo_pago`
  ADD PRIMARY KEY (`id_metodo_pago`);

--
-- Indices de la tabla `pedidos_usuario`
--
ALTER TABLE `pedidos_usuario`
  ADD PRIMARY KEY (`id_pedido_usuario`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `carrito_usuario`
--
ALTER TABLE `carrito_usuario`
  MODIFY `id_carrito_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT de la tabla `metodo_pago`
--
ALTER TABLE `metodo_pago`
  MODIFY `id_metodo_pago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `pedidos_usuario`
--
ALTER TABLE `pedidos_usuario`
  MODIFY `id_pedido_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
